class RockPaperScissors

  # Exceptions this class can raise:
  class NoSuchStrategyError < StandardError ; end

  def self.winner(player1, player2)
    unless 'RPS'.include? player1[1] and 'RPS'.include? player2[1]
      raise NoSuchStrategyError, 'Strategy must be one of R,P,S'
    end

    winner = player1

    if    player1[1].eql? 'R'
      winner = player2 if player2[1].eql? 'P'
    elsif player1[1].eql? 'S'
      winner = player2 if player2[1].eql? 'R'
    else
      winner = player2 if player2[1].eql? 'S'
    end

    winner
  end

  def self.tournament_winner(tournament)
    if tournament[0][0].kind_of? String
      self.winner tournament[0], tournament[1]
    else
      self.winner(
        self.tournament_winner(tournament[0]),
        self.tournament_winner(tournament[1])
      )
    end
  end

end
