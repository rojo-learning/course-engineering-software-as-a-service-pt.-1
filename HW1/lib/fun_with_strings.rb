module FunWithStrings

  def palindrome?
    string = self.gsub(/\W/, '').downcase
    string == string.reverse
  end

  def count_words
    count = Hash.new(0)
    self.gsub(/[^\w\s]/, '').split.each { |w| count[w.downcase] += 1 }
    count
  end

  def anagram_groups
    self.split.group_by{ |e| e.split(//).sort.join }.values
  end

end

# make all the above functions available as instance methods on Strings:

class String
  include FunWithStrings
end
