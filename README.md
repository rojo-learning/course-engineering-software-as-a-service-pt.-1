# Engineering Software as a Service Pt. 1#

This repository contains solutions to the assignments of the [BerkeleyX CS169.1x][0]
course held at the edX platform.

## Assignments and Content ##
* **HW0 Ruby Intro**: Assignment to exercise the use of array's methods, regular expressions, and getters/setters in classes.
* **HW1 More Ruby**: Assignment to exercise the use of string's methods, modules, recursive calls, inheritance, and the dynamic selection of methods using meta-programming with `method_missing`.
* **HW2 Rails Intro**: Continuation of the _RottenPotatoes_ project. This assignment focused in the creation of new features for the app using links and forms in _views_ to pass parameters to the _controller_, the creation of _model_ methods to get sorted and filtered data, and the use of the session hash to store an retrieve values and maintain RESTful URIs when redirecting.
* **HW3 BDD & Cucumber**: Creation of Cucumber _features_, scenarios and steps to test the functionality of the RottenPotatoes app.
* **HW4 BDD, TDD Cycle**: Creation of features and RSpec _unit tests_ to guide the creation of the _search similar movies_ function for RottenPotatoes.

---
This repository contains parts of code based on the original work of Armando Fox
and David Patterson and is used and distributed under the Attribution
Non-Commercial 3.0 CC License.

[0]: https://courses.edx.org/courses/BerkeleyX/CS_CS169.1x/1T2014/8e8cf6e05c8f43749fbac0938f4acbaa/