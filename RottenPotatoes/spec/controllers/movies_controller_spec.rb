
require 'spec_helper'

describe MoviesController do

  describe '#similar' do
    before :each do
      @movie = double('movie')
      allow(@movie).to receive(:director) {'Some Name'}
      Movie.should_receive(:find).and_return(@movie)
    end

    # Happy Path
    context 'with a movie that has director info' do
      before :each do
        @movies = double('movies_array')
        Movie.should_receive(:same_director).with('Some Name').
          and_return(@movies)
      end

      it 'sets the original movie info available to the template' do
        post :similar, {:id => '1'}
        assigns(:movie).should eql(@movie)
      end

      it 'sets the similar movies info available to the template' do
        post :similar, {:id => '1'}
        assigns(:movies).should eql(@movies)
      end

      it 'selects the Similar template for rendering' do
        post :similar, {:id => '1'}
        response.should render_template('similar')
      end
    end

    # Sad Path
    context 'with a movie that does not have director info' do
      before :each do
        allow(@movie).to receive(:director) {''}
        @movie.should receive(:title).and_return('Some Title')
      end

      it 'redirects to the movies page if director is empty' do
        post :similar, {:id => '1'}
        response.should redirect_to movies_path
      end

      it 'redirects to the movies page if director is nil' do
        allow(@movie).to receive(:director) {nil}
        post :similar, {:id => '1'}
        response.should redirect_to movies_path
      end

      it 'sets a notice indicating that there is no director info' do
        post :similar, {:id => '1'}
        notice = "'Some Title' has no director info."
        flash[:notice].should eql(notice)
      end
    end
  end

end
