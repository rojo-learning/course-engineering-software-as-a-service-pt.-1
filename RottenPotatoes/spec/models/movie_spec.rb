
require 'spec_helper'

describe Movie do
  it 'has a RATINGS constant that holds the movie ratings' do
    Movie.const_defined?(:RATINGS).should be
  end

  it 'serves the expected movie ratings' do
    expected_ratings = {
      'G' => 'G', 'PG' => 'PG', 'PG-13' => 'PG-13', 'NC-17' => 'NC-17',
      'R' => 'R'
    }
    Movie::RATINGS.should eql(expected_ratings)
  end

  it 'has a movies method to serve movies with specific ratings/order' do
    Movie.should respond_to(:movies)
  end

  it 'the movies method accepts filters and sort field as argument' do
    Movie.should respond_to(:movies).with(2)
  end

  it 'has a same_director method to serve movies with the same director' do
    Movie.should respond_to(:same_director)
  end

  it 'the same_director method accepts the director\'s name as argument' do
    Movie.should respond_to(:same_director).with(1)
  end

  context 'movies receives certain ratings as filters and a field to order' do
    before :each do
      Movie.create({
        title: 'Star Wars', rating: 'PG', director: 'George Lucas',
        release_date: '1977-05-25'
      })
      Movie.create({
        title: 'THX-1138', rating: 'R', director: 'George Lucas',
        release_date: '1971-03-11'
      })
      Movie.create({
        title: 'Pan\'s Labyrinth', rating: 'PG-13',
        director: 'Guillermo del Toro', release_date: '2011-03-11'
      })
    end

    it 'returns the movies with only the given ratings as filters' do
      filters    = {'PG' => 'PG', 'PG-13' => 'PG-13'}
      sort_field = nil
      movies = Movie.movies(filters, sort_field)
      movies.should have(2).items
    end

    it "should return no movies if there is no one with the given ratings" do
      filters    = {'G' => 'G'}
      sort_field = nil
      movies = Movie.movies(filters, sort_field)
      movies.should be_empty
    end

    it 'returns the movies sorted by the given field' do
      filters    = {'PG' => 'PG', 'PG-13' => 'PG-13', 'R' => 'R'}
      sort_field = 'title'
      movie = Movie.movies(filters, sort_field)[0]
      movie.title.should eql('Pan\'s Labyrinth')
    end

    it 'returns the movies sorted by id if no sort field is given' do
      filters    = {'PG' => 'PG', 'PG-13' => 'PG-13', 'R' => 'R'}
      sort_field = nil
      movie = Movie.movies(filters, sort_field)[0]
      movie.id.should eql(1)
    end
  end

  context 'same_director receives certain director\'s name' do
    before :each do
      Movie.create({
        title: 'Star Wars', rating: 'PG', director: 'George Lucas',
        release_date: '1977-05-25'
      })
      Movie.create({
        title: 'THX-1138', rating: 'R', director: 'George Lucas',
        release_date: '1971-03-11'
      })
      Movie.create({
        title: 'Pan\'s Labyrinth', rating: 'PG-13',
         director: 'Guillermo del Toro', release_date: '2011-03-11'
      })
    end

    it 'returns all the movies with the same director' do
      movies = Movie.same_director('George Lucas')
      movies.should have(2).items
    end

    it "should return no movies if there is no one of that director" do
      movies = Movie.same_director('John Malkovich')
      movies.should be_empty
    end
  end

end
