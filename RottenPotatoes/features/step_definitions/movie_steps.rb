# Add a declarative step here for populating the DB with movies.
Given /the following movies exist/ do |movies_table|
  movies_table.hashes.each do |movie|
    # each returned element will be a hash whose key is the table header.
    # you should arrange to add that movie to the database here.
    Movie.create!(movie)
  end
end

# Make sure that one string (regexp) occurs before or after another one
# on the same page
Then /I should see "(.*)" before "(.*)"/ do |e1, e2|
  #  ensure that that e1 occurs before e2.
  #  page.body is the entire content of the page as a string.
  page.body.index(e1).should < page.body.index(e2)
end

# Make it easier to express checking or unchecking several boxes at once
#  "When I uncheck the following ratings: PG, G, R"
#  "When I check the following ratings: G"

When /I (un)?check the following ratings: (.*)/ do |uncheck, rating_list|
  # HINT: use String#split to split up the rating_list, then
  # iterate over the ratings and reuse the "When I check..." or
  # "When I uncheck..." steps in lines 89-95 of web_steps.rb
  rating_list.split(', ').each do |rating|
    checkbox = "ratings[#{rating}]"

    if uncheck
      step "I uncheck \"#{checkbox}\""
    else
      step "I check \"#{checkbox}\""
    end
  end
end

# Make sure that one string (regexp) occurs or not occurs on the page.
Then /I should(n't)? see the following ratings: (.*)/ do |negative, rating_list|
  rating_list.split(', ').each do |rating|
    if negative
      page.body.include?("<td>#{rating}</td>").should be_false
    else
      page.body.include?("<td>#{rating}</td>").should  be_true
    end
  end
end

Then /I should see all the movies/ do
  # dirty. should be fixed whith has_xpath
  page.body.scan(/<tr>/).length.should equal(Movie.all.length + 1)
end

Then /^the director of "(.*?)" should be "(.*?)"$/ do |arg1, arg2|
  page.body.match /.*#{arg1}.*#{arg2}/
end
