
class Movie < ActiveRecord::Base
  attr_accessible :title, :rating, :description, :release_date, :director

  # List of Ratings used to classify the movies.
  RATINGS = Hash[%w(G PG PG-13 NC-17 R).map {|e| [e, e]}]

  # Set of Movies according to the given parameters.
  def self.movies(filters, sort_field)
    self.where({:rating => filters.values}).order(sort_field)
  end

  def self.same_director(director)
    self.where({:director => director})
  end
end
