
class MoviesController < ApplicationController

  def index

    # Sets a redirection flag if a parameter is recalled from the session.
    def setup(param, default, redirect)
      if params[param.to_sym]
        session[param.to_sym] = params[param.to_sym]
        [params[param.to_sym], redirect]
      elsif session[param.to_sym]
        [session[param.to_sym], true]
      else
        [default, redirect]
      end
    end

    filters, redirect = setup('ratings', Movie::RATINGS, false)
    sort_by, redirect = setup('sort_by', nil, redirect)

    if redirect
      flash.keep
      redirect_to ratings: filters, sort_by: sort_by
    end

    @ratings = Movie::RATINGS
    @filters = filters
    @movies  = Movie.movies filters, sort_by
  end

  def show
    @movie = Movie.find params[:id]
  end

  def new; end

  def create
    @movie = Movie.create! params[:movie]
    flash[:notice] = "#{@movie.title} was succesfully created!"

    redirect_to movies_path
  end

  def edit
    @movie = Movie.find params[:id]
  end

  def update
    @movie = Movie.find params[:id]
    @movie.update_attributes! params[:movie]
    flash[:notice] = "#{@movie.title} was updated!"

    redirect_to movies_path
  end

  def destroy
    @movie = Movie.find params[:id]
    flash[:notice] = "'#{title}' was deleted!"
    @movie.destroy

    redirect_to movies_path
  end

  def similar
    @movie = Movie.find(params[:id])

    if @movie.director.to_s.empty?
      flash[:notice] = "'#{@movie.title}' has no director info."
      redirect_to movies_path
    end

    @movies = Movie.same_director(@movie.director)
  end
end
