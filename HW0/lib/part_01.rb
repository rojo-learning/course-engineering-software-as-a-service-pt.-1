# Define a method sum which takes an array of integers as an argument and
# returns the sum of its elements. For an empty array it should return zero.

def sum(ints)
  ints.inject(0, :+)
end

# Define a method max_2_sum which takes an array of integers as an argument and
# returns the sum of its two largest elements. For an empty array it should
# return zero. For an array with just one element, it should return tha
# element.

def max_2_sum(ints)
  ints.empty? ? 0 : ints.sort![-1] + (ints[-2] or 0)
end


# Define a method sum_to_n? which takes an array of integers and an additional
# integer, n, as arguments and returns true if any two distinct elements in the
# array of integers sum to n. An empty array or single element array should both
# return false.

def sum_to_n?(ints, n)
  ints.combination(2).any? { |a,b| a + b == n}
end
