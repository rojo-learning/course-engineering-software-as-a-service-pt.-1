
require_relative '../lib/part_03'

describe BookInStock do

  context "class is correctly instanced"
    book = BookInStock.new('9780525457589', 9.2)

    it "sets isbn on construction" do
      book.isbn.should eq '9780525457589'
    end

    it "sets price on construction" do
      book.price.should be_within(0.1).of(9.2)
    end

    describe ".price_as_string" do
    it "returns a correctly formatted price" do
      book.price_as_string.should eq '$9.20'
    end
  end

  context "initialization arguments are not valid"
    isbn   = '9780525457589' # valid isbn
    xisbn  = ''              # invalid isbn
    price  = 9.2             # valid price
    xprice = 0               # invalid price

    it "raises AgumentError if isbn is empty" do
      expect { BookInStock.new(xisbn, price) }.to raise_error ArgumentError
    end

    it "raises AgumentError if price is zero or less" do
      expect { BookInStock.new(isbn, xprice) }.to raise_error ArgumentError
    end

end
