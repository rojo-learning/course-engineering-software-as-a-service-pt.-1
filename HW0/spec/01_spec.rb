
require_relative '../lib/part_01'

describe "sum" do
  it "returns zero with an empty array" do
    sum([]).should be 0
  end

  it "returns the sum of an array's elements" do
    sum([1,2,3,4,5]).should be 15
  end
end

describe "max_2_sum" do
  it "returns zero with an empty array" do
    max_2_sum([]).should be 0
  end

  it "returns the element of a single element array" do
    n = rand(100)
    max_2_sum([n]).should be n
  end

  it "returns the sum of 2 elements of a 2 element array" do
    n = rand(100)
    m = rand(100)
    max_2_sum([n,m]).should be n + m
  end

  it "returns 5 with [1,2,3]" do
    max_2_sum([1,2,3]).should be 5
  end

  it "returns 6 with [1,3,2,3]" do
    max_2_sum([1,3,2,3]).should be 6
  end
end

describe "sum_to_n?" do
  it "returns false with an empty array" do
    sum_to_n?([], rand(10)).should be_false
  end

  it "returns false with an array of length 1" do
    sum_to_n?([rand(10)], rand(10)).should be_false
  end

  it "returns true with [0,1,2] and 1" do
    sum_to_n?([0,1,2], 1).should be_true
  end

  it "returns true with [0,1,2] and 3" do
    sum_to_n?([0,1,2], 3).should be_true
  end

  it "returns false with [0,1,2] and 0" do
    sum_to_n?([0,1,2], 0).should be_false
  end

  it "returns true with [3,4,5,6] and 10" do
    sum_to_n?([3,4,5,6], 10).should be_true
  end

  it "returns false with [3,4,5,6] and 12" do
    sum_to_n?([3,4,5,6], 12).should be_false
  end
end
