
require_relative '../lib/part_02'

describe "hello" do
  it "greets the given name" do
    hello("David").should eq "Hello, David"
  end
end

describe "starts_with_consonant?" do
  it "returns false with an empty string" do
    starts_with_consonant?("").should be_false
  end

  it "returns false with a string starting with white space" do
    starts_with_consonant?(" ").should be_false
  end

  it "returns false with a string starting with a digit" do
    starts_with_consonant?("8igth").should be_false
  end

  it "returns false with a string starting with a symbol" do
    starts_with_consonant?("?mark").should be_false
  end

  it "returns false with a string starting with a lowercase vocal" do
    starts_with_consonant?("aword").should be_false
  end

  it "returns false with a string starting with an uppercase vocal" do
    starts_with_consonant?("Aword").should be_false
  end

  it "returns true with a string starting with a lowercase consonant" do
    starts_with_consonant?("word").should be_true
  end

  it "returns true with a string starting with an uppercase consonant" do
    starts_with_consonant?("Great").should be_true
  end
end

describe "binary_multiple_of_4?" do
  it "returns false with an empty string" do
    binary_multiple_of_4?("").should be_false
  end

  it "returns false with a string not starting with 0 or 1" do
    binary_multiple_of_4?(" ").should be_false
  end

  it "returns false with a string containing other character than 0 or 1" do
    binary_multiple_of_4?("011x010").should be_false
  end

  it "returns false with a non valid 0b formatted string" do
    binary_multiple_of_4?("0b11x010").should be_false
  end

  it "returns false with a valid binary string not multiple of 4" do
    binary_multiple_of_4?("011010").should be_false
  end

  it "returns false with a valid 0b formatted string not multiple of 4" do
    binary_multiple_of_4?("0b11010").should be_false
  end

  it "returns true with a valid binary string multiple of 4" do
    binary_multiple_of_4?("0110100").should be_true
  end

  it "returns true with a valid 0b formatted string multiple of 4" do
    binary_multiple_of_4?("0b110100").should be_true
  end
end
